# Outliners, note-taking apps and everything

> An outliner (or outline processor) is a specialized type of text editor (word processor) used to create and edit outlines, which are text files which have a tree structure, for organization. Textual information is contained in discrete sections called "nodes", which are arranged according to their topic–subtopic (parent–child) relationships, like the members of a family tree. When loaded into an outliner, an outline may be collapsed or expanded to display as few or as many levels as desired.
>
> &mdash; [From Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Outliner)

There's considerable overlap between outliners and note-taking apps. Also many of them have features for tracking time, task lists and so on.

## Outliners

- The 800-pound gorilla of outliners is [Org Mode](https://orgmode.org/).
- [OutWiker](https://jenyay.net/Outwiker/English), a mix between outliners and wikis, a desktop application.

Some are only kept around by nostalgia:

- [hnb](http://hnb.sourceforge.net/) <q>a curses program to structure many kinds of data in one place</q> ancient, abandoned and awkward

### Unusual

- [TreeSheets](https://strlen.com/treesheets/) <q>Free Form Data Organizer
(Hierarchical Spreadsheet)</q> a pretty cool concept it turns out, and in active development as of late 2021
- [TreeLine](https://treeline.bellz.org/) is arguably more of a personal database (see review below) with nested records; still developed as of late 2020

### Interoperable

The big problem with most outliners appears to be that they insist on using their own proprietary file formats instead of common options (see below). These are an exception:

- [Scrunch Edit](https://ctrl-c.club/~nttp/toys/scrunch/), a two-pane outliner for Markdown and org files
- [OutNoted](https://ctrl-c.club/~nttp/toys/outnoted/), an outline note-taking editor

Also several mobile outliners use org files.

### Mobile

- [Orgzly](http://www.orgzly.com/): Outliner using org files as an interchange format. It helps to know a few things about the underlying file format, for example how to set a notebook title and what drawers are. Cool and useful, but ultimately too clunky.

### File formats

- Org files are the underlying format of Org Mode. While complex, a decent subset can be parsed easily enough. Compatible apps include Orgzly and OutNoted.
- [OPML](http://opml.org/) is an old and much maligned format, nowadays mostly used by RSS readers to exchange lists of feeds. OutNoted and hnb can use it.

### Links

- [An outliner done right](https://felix.plesoianu.ro/blog/outliner-done-right.html): a review of TreeLine
- [The outline of a game](https://notimetoplay.org/engines/game-outlines.html)
- [OutlinerSoftware.com](https://outlinersoftware.com/), an active forum (as of mid-2022)

## Note-taking apps

- [Zim](https://zim-wiki.org/) <q>A Desktop Wiki</q> is arguably an outliner as well; very popular and friendly, but limited

### Web-based

- [TiddlyWiki](https://tiddlywiki.com/) <q>a non-linear personal web notebook</q>
- [Domino](https://kool.tools/domino/#0,0) <q>a tool for collaging thoughts</q>

### Mobile

- [Markor](https://gsantner.net/project/markor.html) <q>Markdown Editor, todo.txt, Android app</q>

Also there's a set of mobile apps for note-taking that all seem to work the same way, and only differ in the amount of features they offer. All are open source and available on F-Droid. Does this have a name?

- Notally
- Another Notes App
- [Quillnote](https://qosp.org/#/)

The basic principle appears to be a grid (or list) of brief text notes mixed with to-do lists; any of them can have a title, tags and so on, depending. They can be pinned, too. The entire concept works best with larger screens.

They seem similar to [Carnet](https://getcarnet.app/), which is multiplatform.

### Speculative

- [Twine](https://twinery.org/) can be used as a note-taking app, probably
- Collage-based website makers like [HotGlue](https://www.hotglue.me/) or [Multiverse](https://multiverse.plus/) too

## License

This text is under a [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
